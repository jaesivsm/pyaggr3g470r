++++
JARR
++++

Presentation
============

`JARR (Just Another RSS Reader) <https://github.com/JARR-aggregator/JARR>`_ is a
web-based news aggregator.

Main features
=============

* can be easily deployed on Heroku or on a traditional server;
* multiple users can use a JARR instance;
* a RESTful API to manage your articles (or connect your own crawler);
* data liberation: export and import all your account with a JSON file;
* export and import feeds with OPML files;
* export articles to HTML;
* favorite articles;
* detection of inactive feeds;
* share articles with Google +, Pinboard and reddit.

The core technologies are `Flask <http://flask.pocoo.org>`_,
`asyncio <https://www.python.org/dev/peps/pep-3156/>`_ and
`SQLAlchemy <http://www.sqlalchemy.org>`_.

Python 3.5 is recommended.

Documentation
=============

A documentation is available `here <https://jarr.readthedocs.org>`_ and provides
different ways to install JARR.

Internationalization
====================

JARR is translated into English and French.

Donation
========

If you wish and if you like *JARR*, you can donate via bitcoin
`1GVmhR9fbBeEh7rP1qNq76jWArDdDQ3otZ <https://blockexplorer.com/address/1GVmhR9fbBeEh7rP1qNq76jWArDdDQ3otZ>`_.
Thank you!

License
=======

`JARR <https://github.com/JARR-aggregator/JARR>`_
is under the `GNU Affero General Public License version 3 <https://www.gnu.org/licenses/agpl-3.0.html>`_.

Contact
=======

`My home page <https://www.cedricbonhomme.org>`_.
